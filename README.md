# Sector Contrib

[Sector](https://www.sector.org.nz/) is a Drupal 9 distribution (or install profile) built and maintained by [Sparks Interactive](https://www.sparksinteractive.co.nz). The default Sector README is in [README.Sector.md](README.Sector.md).

This repo is a project built from Sector's composer project template. It is intended to document and facilitate the process of contributing changes back to Sector.

## Components

There are additional components here which aren't part of Sector's base package.

## Set up to hack on Sector project

- This project is in ~/Projects/sector-contrib and is the working Lando / Drupal site.
- Clone https://git.drupalcode.org/project/sector.git (or your fork) to ~/Projects/sector-distribution - this is the Drupal profile.
- Clone https://github.com/sparksi/sector-distribution (or your fork) to ~/Projects/sector-project - this is the Composer project template.

In Lando, repo references will be accessed from `/user/Projects` which is mounted from your host OS `~/Projects`. If you remove the repository `local.sector-distribution` from composer.json, the build process will use Packagist's version.

## Install

```
cp env.example .env
lando drush -y site-install sector --db-url=mysql://drupal9:drupal9@database/drupal9
```
